<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Caro game</title>
		<style type="text/css" media="screen">
		body {
			display: flex;
			flex-direction: column;
			align-items: center;

		}
		.board {
		    display: flex;
 			flex-wrap: wrap;
		}
		.span {
			display: flex;
		    border: 1px solid #ccc;
		    box-sizing: border-box;
		    align-items: center;
		    justify-content: center;
		    color: #fff;
		}	
		</style>
	</head>
	<body>
		<h1>Caro Game</h1>
		<div class="board" id="div1" style="width: 1500px;">
		</div>
		<button onclick="caro('div1')">Chơi lại</button>
		<!-- <script type="text/javascript">
			var caro = (function() {
			    return this;
			})();

			(function(w) {
		        w.caro = function(id) {
		            var ban, col, width, A, B, colNum, value;
		            ban = document.getElementById(id);
		            col = 15;
		            width = 30;
		            A = 'X';
		            B = 'O';

		            function createChessboard() {
		                ban.style.width = width * col + "px";
		                ban.style.height = width * col + "px";
		                var html = '';
		                for (var i = 0; i < col; i++) {
		                    for (var j = 0; j < col; j++) {
		                        html += `<span class="span" style="width:${width}px; height:${width}px;" cols="${i} ${j}"></span>`;
		                    }
		                }
		                ban.innerHTML = html;
		                ban.style.pointerEvents = "auto";
		                play(ban);
		            }

		            function play() {
		                var flag = true;
		                var start = 0;
		                ban.querySelectorAll("span").forEach(function(i, j) {
		                    i.onclick = function() {
		                        colNum = this.getAttribute("cols");
		                        if (this.innerHTML == '') {
		                            if (flag == true) {
		                                this.style.background = 'red';
		                                this.innerHTML = A;
		                                flag = false;
		                            } else {
		                                this.style.background = '#777';
		                                this.innerHTML = B;
		                                flag = true;
		                            };
		                        } else {
		                            alert("Xin mời đánh ô khác!")
		                        }
		                        value = this.innerHTML;
		                        start++;
		                        if (start >= 0) {

		                            checkWin(colNum, value);
		                        }

		                    }
		                });
		            }

		            function checkWin(i, value) {
				        //check ngang
				        var ngangA = i.split(" ");
				        var arrNgang = [];
				        arrNgang.push(i);

				        while (true) {
				            if (parseInt(ngangA[1]) < col - 1) {
				                ngangA[1] = parseInt(ngangA[1]) + 1;
				                if (ban.querySelector("span[cols='" + ngangA[0] + " " + ngangA[1] + "']").innerHTML === value) {
				                    arrNgang.push(ngangA[0] + " " + ngangA[1]);
				                } else {
				                    break;
				                }
				            } else {
				                break;
				            }
				        }

				        var ngangB = i.split(" ");
				        while (true) {
				            if (parseInt(ngangB[1]) > 0) {
				                ngangB[1] = parseInt(ngangB[1]) - 1;
				                if (ban.querySelector("span[cols='" + ngangB[0] + " " + ngangB[1] + "']").innerHTML === value) {
				                    arrNgang.push(ngangB[0] + " " + ngangB[1]);
				                } else {
				                    break;
				                }
				            } else {
				                break;
				            }
				        }
				        confirmWin(arrNgang, value);

				        //check dọc
				        var docA = i.split(" ");
				        var arrDoc = [];
				        arrDoc.push(i);
				        while (true) {
				            if (parseInt(docA[0]) < col - 1) {
				                docA[0] = parseInt(docA[0]) + 1;
				                if (ban.querySelector("span[cols='" + docA[0] + " " + docA[1] + "']").innerHTML === value) {
				                    arrDoc.push(docA[0] + " " + docA[1]);
				                } else {
				                    break;
				                }
				            } else {
				                break;
				            }
				        }

				        var docB = i.split(" ");
				        while (true) {
				            if (parseInt(docB[0]) > 0) {
				                docB[0] = parseInt(docB[0]) - 1;
				                if (ban.querySelector("span[cols='" + docB[0] + " " + docB[1] + "']").innerHTML === value) {
				                    arrDoc.push(docB[0] + " " + docB[1]);
				                } else {
				                    break;
				                }
				            } else {
				                break;
				            }
				        }
				        confirmWin(arrDoc, value);

				        //check chéo i tăn
				        var cheoA = i.split(" ");
				        var arrCheoAB = [];
				        arrCheoAB.push(i);
				        while (true) {
				            if (parseInt(cheoA[0]) < col - 1 && parseInt(cheoA[1]) < col - 1) {
				                cheoA[0] = parseInt(cheoA[0]) + 1;
				                cheoA[1] = parseInt(cheoA[1]) + 1;
				                if (ban.querySelector("span[cols='" + cheoA[0] + " " + cheoA[1] + "']").innerHTML === value) {
				                    arrCheoAB.push(cheoA[0] + " " + cheoA[1]);
				                } else {
				                    break;
				                }
				            } else {
				                break;
				            }
				        }
				        //check chéo i giảm j giảm
				        var cheoB = i.split(" ");
				        while (true) {
				            if (parseInt(cheoB[0]) > 0 && parseInt(cheoB[1]) > 0) {
				                cheoB[0] = parseInt(cheoB[0]) - 1;
				                cheoB[1] = parseInt(cheoB[1]) - 1;
				                if (ban.querySelector("span[cols='" + cheoB[0] + " " + cheoB[1] + "']").innerHTML === value) {
				                    arrCheoAB.push(cheoB[0] + " " + cheoB[1]);
				                } else {
				                    break;
				                }
				            } else {
				                break;
				            }
				        }
				        confirmWin(arrCheoAB, value);

				        //check chéo i tăng j giảm
				        var cheoC = i.split(" ");
				        var arrCheoCD = [];
				        arrCheoCD.push(i);
				        while (true) {
				            if (parseInt(cheoC[0]) < col - 1 && parseInt(cheoC[1]) > 0) {
				                cheoC[0] = parseInt(cheoC[0]) + 1;
				                cheoC[1] = parseInt(cheoC[1]) - 1;
				                if (ban.querySelector("span[cols='" + cheoC[0] + " " + cheoC[1] + "']").innerHTML === value) {
				                    arrCheoCD.push(cheoC[0] + " " + cheoC[1]);
				                } else {
				                    break;
				                }
				            } else {
				                break;
				            }
				        }
				        //check chéo i giảm j tăng
				        var cheoD = i.split(" ");
				        while (true) {
				            if (parseInt(cheoD[0]) > 0 && parseInt(cheoD[1]) < col - 1) {
				                cheoD[0] = parseInt(cheoD[0]) - 1;
				                cheoD[1] = parseInt(cheoD[1]) + 1;
				                if (ban.querySelector("span[cols='" + cheoD[0] + " " + cheoD[1] + "']").innerHTML === value) {
				                    arrCheoCD.push(cheoD[0] + " " + cheoD[1]);
				                } else {
				                    break;
				                }
				            } else {
				                break;
				            }
				        }
				        confirmWin(arrCheoCD, value);

				        function confirmWin(arr, value) {
				            if (arr.length >= 5) {
				                arr.forEach(function(i, j) {
				                    ban.querySelector("span[cols='" + i + "']").style.background = "green";
				                });
				                var cof;
				                setTimeout(function() {
				                    cof = confirm(value + " là người thắng! có muốn chơi lại không?");
				                    if (cof) {
				                        createChessboard(ban.className);
				                    } else {
				                        ban.style.pointerEvents = "none";
				                    }
				                }, 8);
				            }
				        }
				    }

				    return createChessboard();
		        }
			})(caro);

			caro("div1");
		</script> -->
		<script>
			var caro = function() {
			    return this
			}();
			(function(k) {
			    k.caro = function(k) {
			        function l() {
			            d.style.width = h * f + "px";
			            d.style.height = h * f + "px";
			            for (var b = "", c = 0; c < f; c++)
			                for (var g = 0; g < f; g++) b += '<span class="span" style="width:' + h + "px; height:" + h + 'px;" cols="' + c + " " + g + '"></span>';
			            d.innerHTML = b;
			            d.style.pointerEvents = "auto";
			            p(d)
			        }

			        function p() {
			            var b = !0,
			                c = 0;
			            d.querySelectorAll("span").forEach(function(d, a) {
			                d.onclick = function() {
			                    m = this.getAttribute("cols");
			                    "" == this.innerHTML ? 1 == b ? (this.style.background = "red", this.innerHTML = q, b = !1) : (this.style.background = "#777",
			                        this.innerHTML = r, b = !0) : alert("Xin m\u1eddi \u0111\u00e1nh \u00f4 kh\u00e1c!");
			                    n = this.innerHTML;
			                    c++;
			                    0 <= c && t(m, n)
			                }
			            })
			        }

			        function t(b, c) {
			            function g(a, b) {
			                if (5 <= a.length) {
			                    a.forEach(function(a, b) {
			                        d.querySelector("span[cols='" + a + "']").style.background = "green"
			                    });
			                    var c;
			                    setTimeout(function() {
			                        (c = confirm(b + " l\u00e0 ng\u01b0\u1eddi th\u1eafng! c\u00f3 mu\u1ed1n ch\u01a1i l\u1ea1i kh\u00f4ng?")) ? l(d.className): d.style.pointerEvents = "none"
			                    }, 8)
			                }
			            }
			            var a = b.split(" "),
			                e = [];
			            for (e.push(b);;)
			                if (parseInt(a[1]) < f - 1)
			                    if (a[1] =
			                        parseInt(a[1]) + 1, d.querySelector("span[cols='" + a[0] + " " + a[1] + "']").innerHTML === c) e.push(a[0] + " " + a[1]);
			                    else break;
			            else break;
			            for (a = b.split(" ");;)
			                if (0 < parseInt(a[1]))
			                    if (a[1] = parseInt(a[1]) - 1, d.querySelector("span[cols='" + a[0] + " " + a[1] + "']").innerHTML === c) e.push(a[0] + " " + a[1]);
			                    else break;
			            else break;
			            g(e, c);
			            a = b.split(" ");
			            e = [];
			            for (e.push(b);;)
			                if (parseInt(a[0]) < f - 1)
			                    if (a[0] = parseInt(a[0]) + 1, d.querySelector("span[cols='" + a[0] + " " + a[1] + "']").innerHTML === c) e.push(a[0] + " " + a[1]);
			                    else break;
			            else break;
			            for (a =
			                b.split(" ");;)
			                if (0 < parseInt(a[0]))
			                    if (a[0] = parseInt(a[0]) - 1, d.querySelector("span[cols='" + a[0] + " " + a[1] + "']").innerHTML === c) e.push(a[0] + " " + a[1]);
			                    else break;
			            else break;
			            g(e, c);
			            a = b.split(" ");
			            e = [];
			            for (e.push(b);;)
			                if (parseInt(a[0]) < f - 1 && parseInt(a[1]) < f - 1)
			                    if (a[0] = parseInt(a[0]) + 1, a[1] = parseInt(a[1]) + 1, d.querySelector("span[cols='" + a[0] + " " + a[1] + "']").innerHTML === c) e.push(a[0] + " " + a[1]);
			                    else break;
			            else break;
			            for (a = b.split(" ");;)
			                if (0 < parseInt(a[0]) && 0 < parseInt(a[1]))
			                    if (a[0] = parseInt(a[0]) - 1, a[1] = parseInt(a[1]) -
			                        1, d.querySelector("span[cols='" + a[0] + " " + a[1] + "']").innerHTML === c) e.push(a[0] + " " + a[1]);
			                    else break;
			            else break;
			            g(e, c);
			            a = b.split(" ");
			            e = [];
			            for (e.push(b);;)
			                if (parseInt(a[0]) < f - 1 && 0 < parseInt(a[1]))
			                    if (a[0] = parseInt(a[0]) + 1, a[1] = parseInt(a[1]) - 1, d.querySelector("span[cols='" + a[0] + " " + a[1] + "']").innerHTML === c) e.push(a[0] + " " + a[1]);
			                    else break;
			            else break;
			            for (a = b.split(" ");;)
			                if (0 < parseInt(a[0]) && parseInt(a[1]) < f - 1)
			                    if (a[0] = parseInt(a[0]) - 1, a[1] = parseInt(a[1]) + 1, d.querySelector("span[cols='" + a[0] + " " + a[1] + "']").innerHTML ===
			                        c) e.push(a[0] + " " + a[1]);
			                    else break;
			            else break;
			            g(e, c)
			        }
			        var m, n;
			        var d = document.getElementById(k);
			        var f = 15;
			        var h = 30;
			        var q = "X";
			        var r = "O";
			        return l()
			    }
			})(caro);
			caro("div1");
		</script>
	</body
</html>