<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Home page</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="css/index.css">
		<link rel="stylesheet" href="css/lifestyle.css">
		<link rel="stylesheet" href="css/photodiary.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="js/index.js" async="true"></script>
	</head>
	<body>
		<div id="wrapper">
			<!-- header top -->
			<header id="header" class="container">
				<?php require_once("partials/header.html") ?>
			</header>
			
			<!-- banner -->
			<section class="container banner">
				<img src="images/banner.jpg" alt="banner" class=" banner__img">
			</section>

			<!-- main content -->
			<?php
				$action = isset($_GET["action"])? $_GET["action"] : '';
				if($action != '') {
					require_once("layouts/" . $_GET["action"] . ".php");
				} else {
					require_once ("layouts/homepage.php");
				}
			 ?>

			<!-- footer	 -->
			<footer class="footer">
				<?php require_once("partials/footer.html") ?>
			</footer>
		</div>
	</body>
</html>