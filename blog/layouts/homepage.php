<main class="main">
	<!-- main top -->
	<div class="main-container main__top">
		<section class="text-story">
			<span class="text-story__cate font-Ub-light">PHOTODIARY</span>
			<h2 class="text-story__title font-PD-regular">
				The perfect weekend getaway 
			</h2>
			<p class="text-story__content font-PD-regular">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
			</p>
			<span class="text-story__note font-Ub-light">LEAVE A COMMENT</span>
		</section>
		<div class="box-story">
			<?php require("partials/boxcontent.html") ?>
		</div>
	</div>

	<!-- form subcrible -->
	<?php require_once("partials/form-sub.html") ?>
	
	<!-- box below -->
	<div class="main-container main__top">
		<div class="box-story">
			<div class="article">
				<a href="#" title="title-slug" class="article__img">
					<img src="images/boxcontent5.jpg" alt="title-slug">
				</a>
				<a href="#" class="article__cate font-Ub-light">lifestyle</a>
				<h2 class="article__title">
					<a href="#" title="title-slug" class="font-PD-regular">Top 10 song for running </a>
				</h2>
				<p class="article__desc font-PD-regular">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris </p>
			</div>

			<div class="article">
				<a href="#" title="title-slug" class="article__img">
					<img src="images/boxcontent6.jpg" alt="title-slug">
				</a>
				<a href="#" class="article__cate font-Ub-light">lifestyle</a>
				<h2 class="article__title">
					<a href="#" title="title-slug" class="font-PD-regular">Cold winter days</a>
				</h2>
				<p class="article__desc font-PD-regular">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris </p>
			</div>
		</div>
		<div class="main__load-more">
			<button class="main__load-more--btn font-PD-regular">Load more</button>
		</div>
	</div>
</main>