<main class="main">
	<!-- main top -->
	<div class="main-container main__top">
		<section class="text-story">
			<span class="text-story__cate font-Ub-light">PHOTODIARY</span>
			<h2 class="text-story__title font-PD-regular">
				The perfect weekend getaway 
			</h2>
			<p class="text-story__content font-PD-regular">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.

				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
			</p>
			<img src="images/lifestyle/1.jpg" class="life-style__img" alt="#">
			<img src="images/lifestyle/2.jpg" class="life-style__img" alt="#">
			<img src="images/lifestyle/3.jpg" class="life-style__img" alt="#">
			<blockquote class="font-PD-italic">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias aperiam velit perspiciatis animi cum, qui modi labore totam cumque accusantium aut sed eius, quia obcaecati earum assumenda necessitatibus rem ad provident harum. Fuga non quasi minus, magni. Repellendus reprehenderit porro laboriosam libero ut alias voluptatem consectetur, hic soluta culpa modi tempora quam blanditiis veritatis voluptatum et, dolor magni optio. Asperiores adipisci, magni molestias libero in et modi assumenda nesciunt sapiente error necessitatibus sed, accusantium itaque fugit, labore. Ipsam vitae est, quas ipsum quos hic reprehenderit perferendis. Aut nesciunt fuga quae odit laborum excepturi inventore consequuntur, deserunt rerum dolorum optio, sit.
			</blockquote>
			<p class="font-PD-regular">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem
			</p>
			<div class="life-style__share">
				<span class="font-Ub-light">SHARE</span>
				<a href="#"><img src="css/images/fb.png" alt=""></a>
				<a href="#"><img src="css/images/instagram.png" alt=""></a>
				<a href="#"><img src="css/images/twitter.png" alt=""></a>
			</div>
		</section>
	</div>
	<div class="slide main__slide">
		<?php require_once("partials/slide.html") ?>
	</div>

	<section class="comment main__comment">
		<?php require_once("partials/comment.html") ?>
	</section>
</main>