<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>index</title>
		<link rel="stylesheet" href="css/index.css">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="./js/jquery-3.1.1.min.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<!-- header -->
			<header class="header">
				<?php require_once("layouts/partials/header.html") ?>
			</header>

			<!-- main -->
			<main class="main">
				<?php require_once("layouts/partials/box.html") ?>
			</main>
		</div>
	</body>
</html>