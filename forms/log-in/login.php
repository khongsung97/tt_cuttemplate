<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Form Template</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="css/login.css">
	</head>
	<body>
		<div class="wrapper">
			<div class="map" id="map">
			</div>
			<div class="contact">
				<div class="header">
					<span class="header__title">Sign In</span>
				</div>
				<form action="process.php" method="POST" class="form" id="form">
					<div class="item" data-validate="Email address is required : ex@abc.xy">
						<span class="item__label">Emai:</span>
						<input type="text" name="email" class="item__input" placeholder="Enter email address">
						<span class="item__input-focus"></span>
					</div>
					<div class="item" data-validate="Phone is required, type is number">
						<span class="item__label">Password:</span>
						<input type="password" name="password" class="item__input" placeholder="Enter password">
						<span class="item__input-focus"></span>
					</div>

					<div class="btn">
						<div class="btn btn-wrap">
							<input type="submit" name="submit" class="btn__submit" value=" ">
							<span class="btn__icon">Log in</span>
						</div>
					</div>
				</form>
			</div>
			<?php
				if(isset($_GET['err']) && $_GET['err'] === 'fail') {
					echo('<script>alert("Sai tên tài khoản hoặc mật khẩu");</script>');
				}
			?>
		</div>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="js/jquery.validate.min.js" async="true"></script>	
		<script src="js/login.js" async="true"></script>
	</body>
</html>