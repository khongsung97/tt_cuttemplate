<?php 
	try {
		function connectDB() {
			$user = 'root';
			$pass = '12345678';
			$conn = new PDO('mysql:host=localhost;dbname=duan;charset=UTF8',$user,$pass);
			$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			return $conn;
		}

		function getUser($email, $pass) {
			$conn = connectDB();
			$stmt = $conn->prepare("SELECT * FROM users WHERE Email = ? AND Password = ?");
			$stmt->bindParam(1,$email);
			$stmt->bindParam(2,$pass);
			$stmt->setFetchMode(PDO::FETCH_OBJ);
			$stmt->execute();
			return $stmt->fetch();
		}

		if($_POST['submit']) {
			if(getUser($_POST["email"], $_POST["password"])) {
				echo("success");
			} else {
				header('location: login.php?err=fail');
			}
		}
		
	} catch (PDOExeption $e) {
		echo $e->getMessage();
	}
 ?>