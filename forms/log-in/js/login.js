
$(document).ready(function() {
	$('#form').validate({
		rules : {
			email: {
				required: true,
				email: true
			},
			password: {
				required: true,
				minlength: 6
			}
		},
		messages: {
			email: {
				required: "Vui lòng nhập địa chỉ email",
				email: 'Email không hợp lệ'
			},
			password: {
				required: 'Vui lòng nhập mật khẩu',
				minlength: 'Mật khẩu phải lớn hơn 6 ký tự'
			}
		}
	});	
	
})