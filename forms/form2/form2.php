<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Form Template</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="css/form2.css">
		<link href="https://fonts.googleapis.com/css?family=Poppins:400,700,800" rel="stylesheet">
		<link rel="stylesheet" href="https://colorlib.com/etc/cf/ContactFrom_v4/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="js/form2.js" async="true"></script>	
	</head>
	<body style="margin: 0;">
		<div class="container">
			<div class="form_wrapper">
				<form action="process.php" method="post" class="form">
					<span class="form__header">Contact Us</span>
					<div class="form__item" data-validate="Name is required">
						<input type="text" class="input" name="name" id="name">
						<span class="focus-input" data-title="Your Name"></span>
					</div>
					<div class="form__item" data-validate="Email address is required : ex@abc.xy">
						<input type="text" class="input" name="email" id="email">
						<span class="focus-input" data-title="Your Email Address"></span>
					</div>
					<div class="form__item"  data-validate="Message is required">
						<textarea name="message" id="message" class="input textarea"></textarea>
						<span class="focus-input" data-title="Message"></span>
					</div>
					<div class="form__submit">
						<div class="overflow">
						</div>
						<input type="submit" name="submit" class="btnSubmit" value="Submit">
					</div>
				</form>
			</div>
		</div>
	</body>
</html>