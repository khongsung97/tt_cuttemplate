(function($) {
    "use strict";
    $.fn.validate = function() {
    	let that = this;

        that.find('.btnSubmit').mouseover(function() {
            $('.overflow').css('left', '0px');
        });
        that.find('.btnSubmit').mouseout(function() {
            $('.overflow').css('left', '-350px');
        });

        let name = that.find('input[name="name"]');
        let email = that.find('input[name="email"]');
        let message = that.find('textarea[name="message"]');
        let data = [];

        that.on('submit', function() {
            var check = true;
            if ($(name).val().trim() === '') {
                let validate = "Name is required";
                name.parent().find('.input-validate').html(validate);
                showValidate(name);
                check = false;
            }
            if ($(email).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) === null) {
                let validate = "Email address is required : ex@abc.xy";
                email.parent().find('.input-validate').html(validate);
                showValidate(email);
                check = false;
            }
            if ($(message).val().trim() === '') {
                let validate = "Message is required";
                message.parent().find('.input-validate').html(validate);
                showValidate(message);
                check = false;
            }
            return check;
        });
        that.find('.input').each(function() {
            $(this).focus(function() {
                hideValidate(this);
            });
        });

        function showValidate(input) {
            var thisAlert = $(input).parent();
            $(thisAlert).addClass('alert-validate');
        }

        function hideValidate(input) {
            var thisAlert = $(input).parent();
            $(thisAlert).removeClass('alert-validate');
        }
    }
})(jQuery)

$(document).ready(function() {
	$('.form').validate();
});