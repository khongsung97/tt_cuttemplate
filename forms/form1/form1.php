<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Form Template</title>
		<link rel="stylesheet" href="css/form1.css">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://fonts.googleapis.com/css?family=Poppins:400,700,800" rel="stylesheet">
		<link rel="stylesheet" href="https://colorlib.com/etc/cf/ContactFrom_v4/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="js/form1.js" async="true"></script>	
	</head>
	<body style="margin: 0;">
		<div class="container">
			<div class="form_wrapper">
				<form action="process.php" method="post" class="form">
					<span class="form__header">Say Hello!</span>
					<div class="form__item">
						<label for="name" class="label">Your Name</label>
						<span class="input-validate"></span>
						<input type="text" class="input" name="name" id="name" value="" placeholder="Enter your name" >
						<span class="focus-input"></span>
					</div>
					<div class="form__item">
						<label for="email" class="label">Email</label>
						<span class="input-validate"></span>
						<input type="text" class="input" name="email" id="email" value="" placeholder="Enter your email address" >
						<span class="focus-input"></span>
					</div>
					<div class="form__item">
						<label for="gender" class="label">Gender</label>
						<span class="input-validate"></span>
						<select name="gender" id="gender" class="input select">
							<option value="1" selected>Male</option>
							<option value="0">Female</option>
						</select>
					</div>
					<div class="form__item">
						<label for="message" class="label">Message</label>
						<span class="input-validate"></span>
						<textarea name="message" id="message" class="input textarea" placeholder="Your message here"></textarea>
						<span class="focus-input"></span>
					</div>
					<div class="form__submit">
						<div class="overflow">
						</div>
						<input type="submit" name="submit" class="btnSubmit" value="Submit">
					</div>
				</form>
			</div>
		</div>
	</body>
</html>