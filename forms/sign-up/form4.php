<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Form Template</title>
		<link rel="stylesheet" href="css/form4.css">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://fonts.googleapis.com/css?family=Poppins:400,700,800" rel="stylesheet">
		<link rel="stylesheet" href="https://colorlib.com/etc/cf/ContactFrom_v4/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	</head>
	<body style="margin: 0;">
		<div class="container">
			<div class="form_wrapper">
				<form action="process.php" method="post" class="form" id="form">
					<span class="form__header">Sign up</span>
					<div class="form__item">
						<label for="name" class="label">Your Name</label>
						<span class="input-validate"></span>
						<input type="text" class="input" name="name" id="name" value="" placeholder="Enter your name" >
						<span class="focus-input"></span>
					</div>
					<div class="form__item">
						<label for="email" class="label">Email</label>
						<span class="input-validate"></span>
						<input type="text" class="input" name="email" id="email" value="" placeholder="Enter your email address" >
						<span class="focus-input"></span>
					</div>
					<div class="form__item">
						<label for="password" class="label">Password</label>
						<span class="input-validate"></span>
						<input type="text" class="input" name="password" id="password" value="" placeholder="Enter your password" >
						<span class="focus-input"></span>
					</div>
					<div class="form__item">
						<label for="password_again" class="label">Confirm Password</label>
						<span class="input-validate"></span>
						<input type="text" class="input" name="password_again" id="password_again" value="" placeholder="Confirm password" >
						<span class="focus-input"></span>
					</div>
					<div class="form__submit">
						<div class="overflow">
						</div>
						<input type="submit" name="submit" class="btnSubmit" value="Sign up">
					</div>
				</form>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="js/jquery.validate.min.js" async="true"></script>	
		<script src="js/form4.js" async="true"></script>
	</body>
</html>